package vector_design;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * initial save file function
 */
public class FileMenu extends JMenu {
    public static ArrayList<Line2D> dotList;
    public static ArrayList<Line2D> lineList;
    public static ArrayList<Ellipse2D> ellipseList;
    public static ArrayList<Rectangle2D> rectList;
    public static String content = "";
    public FileMenu(String name){
        super(name);
        VECTool FileToolBar = new VECTool(null);
        dotList = FileToolBar.getDotList();
        lineList = FileToolBar.getLineList();
        ellipseList = FileToolBar.getEllipseList();
        rectList = FileToolBar.getRectList();
    }

    // get coordinates of line
    private String lineToString(Line2D o){

        return "LINE " +(int)o.getX1() +" "+ (int)o.getY1() +" "+ (int)o.getX2() + " "+ (int)o.getY2() + "\n";
    }
    // get coordinates of rectangle
    private String rectToString(Rectangle2D o){
        return "RECTANGLE " + (int)o.getX()  +" "+ (int)o.getY()  +" "+ (int)o.getMaxX() +" " + (int)o.getMaxY() + "\n";
    }
    //get coordinates of dot
    private String dotToString(Line2D o){
        return "DOT " + (int)o.getP1().getX() +" " + (int)o.getP1().getY() +" " +"\n";

    }
    // get coordinates of ellipse
    private  String elpToString(Ellipse2D o){
        return  "ELLIPSE " + (int)o.getX() +" " +(int)o.getY() +" " +(int)o.getMaxX() +" " + (int)o.getMaxY() + "\n";

    }

    //save coordinates to string in content
    public void SaveCOOR(){
        for (Rectangle2D rec: rectList) {
            content+= rectToString(rec);
        }
        System.out.println(content);

        for(Line2D dot:dotList){
            content+= dotToString(dot);
        }
        System.out.println(content);

        for(Ellipse2D elp:ellipseList){
            content+= elpToString(elp);
        }
        System.out.println(content);

        for(Line2D l:lineList){
            content+= lineToString(l);
        }
        System.out.println(content);
    }
    // get lists of coordinates from content
    public String getContent(){
        return content;
    }

}

    class FileOperator extends JMenuItem {
    public FileOperator(String name){
        super(name);
    }

    /* write inputStream to *.vec file
    */
    public void addSaveListener(){
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileMenu FileToolBar = new FileMenu("Save file");

                System.out.println(FileToolBar.getContent());
                FileToolBar.SaveCOOR();
                JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

                jfc.setDialogTitle("Choose a directory to save your file: ");
                jfc.setAcceptAllFileFilterUsed(false);
                FileNameExtensionFilter filter = new FileNameExtensionFilter("*.vec", "vec");
                jfc.addChoosableFileFilter(filter);

                jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                 int returnValue = jfc.showSaveDialog(null);

                if (returnValue == JFileChooser.APPROVE_OPTION) {

                    File vecfile = jfc.getSelectedFile();
                    String path =vecfile.getPath();
                    try {
                        if(!path.toLowerCase().endsWith(".vec")){
                            vecfile = new File(path + ".vec");
                        }
                        FileOutputStream ftos = new FileOutputStream(vecfile);
                        ftos.close();
                        FileWriter fileWriter = new FileWriter(vecfile);
                        fileWriter.write(FileToolBar.getContent());
                        fileWriter.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    if (vecfile.isDirectory()) {
                       System.out.println("You selected the directory: " + vecfile);
                    }
                }
            }
        });
    }
    public void addOpenListenr(){
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileMenu FileToolBar = new FileMenu("Open file");

                JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
                jfc.setDialogTitle("Select a VEC file ");
                jfc.setAcceptAllFileFilterUsed(false);
                FileNameExtensionFilter filter = new FileNameExtensionFilter("*.vec", "vec");
                jfc.addChoosableFileFilter(filter);
                int returnValue = jfc.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                   File selectedFile = jfc.getSelectedFile();
                    System.out.println(selectedFile.getAbsolutePath());
                }
            }
        });
    }
}



