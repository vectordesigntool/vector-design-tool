package vector_design;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;


class DrawShape extends JPanel{
        private Point2D[] points;
        private int pos;
        private final int SIZE = 8;
        private int x, y, x2, y2;
        private boolean draw = false;

        public DrawShape(){
            MouseListener listener = new MouseListener();
            addMouseListener(listener);
            addMouseMotionListener(listener);
            //pos = -1;
       /* points = new Point2D[2];
        points[0] = new Point2D.Double(x, y);
        points[1] = new Point2D.Double(x2, y2);*/

        }

    private void drawLine(Graphics g, double x, double y, double x2, double y2) {
        Graphics2D draw = (Graphics2D) g;
      /* for (Point2D point : points) {
            double pointx = point.getX() - SIZE / 2;
            double pointy = point.getY() - SIZE / 2;
            g2.fill(new Rectangle2D.Double(pointx, pointy, SIZE, SIZE));
        }*/

        Line2D l = new Line2D.Double(x, y, x2, y2);
        l.setLine(x, y, x2, y2);
        draw.draw(l);
    }

        private void drawRect(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
       /* for (Point2D point : points) {
            double pointx = point.getX() - SIZE/2;
            double pointy = point.getY() - SIZE /2;
            g2.fill(new Rectangle2D.Double(pointx, pointy, SIZE, SIZE));
        }*/
            Rectangle2D r = new Rectangle2D.Double();
            r.setFrameFromDiagonal(x , y, x2, y2);
            g2.draw(r);

        }


        private void drawElispe(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
     /*  for (Point2D point : points) {
            double pointx = point.getX() - SIZE / 2;
            double pointy = point.getY() - SIZE / 2;
            g2.fill(new Rectangle2D.Double(pointx, pointy, SIZE, SIZE));
        }*/

            Ellipse2D e = new Ellipse2D.Double(x, y, x2, y2);
            g2.draw(e);
        }

      private class MouseListener extends MouseAdapter {

       public void mousePressed(MouseEvent event) {

          /* int x = event.getX();
            int y = event.getY();

            if (event.isMetaDown()) {
                Graphics graphic = getGraphics();
                //Rectangle rec = getBounds();
                //graphic.clearRect(0, 0, rec.width, rec.height);
                //graphic.drawLine(x, y, x2, y2);
                graphic.drawLine(x, y, x2, y2);
                graphic.dispose();

            } else {
                x2 = x;
                y2 = y;
            }*/
            /*x = x2 = event.getX();
            y = y2 = event.getY();
            repaint();*/

        x = x2 = event.getX();
        y = y2 = event.getY();
        repaint();
          /* Point p = event.getPoint();
            points[0].setLocation(x, y);
            points[1].setLocation(x2, y2);

            for (int i = 0; i < points.length; i++) {

                double x = points[i].getX() - SIZE /2;
                double y = points[i].getY() - SIZE /2;

                Rectangle2D r = new Rectangle2D.Double(x, y, SIZE, SIZE);

                if (r.contains(p)) {

                    pos = i;
                    return;
                }
            }*/


    }


    public void mouseDragged(MouseEvent event) {
           /* int x = event.getX();
            int y = event.getY();
            if (!event.isMetaDown()) {
                Graphics g = getGraphics();
                //g.drawLine(x, y, x2, y2);
                g.drawLine(x, y, x2, y2);
                g.dispose();
                x2 = x;
                y2 = y;

            }*/
           /*x2 = event.getX();
           y2 = event.getY();
           repaint();*/
        x2 = event.getX();
        y2 = event.getY();
         /*   if (pos == -1) {
                return;
            }
            points[pos] = event.getPoint();*/
        repaint();

    }

    public void mouseReleased(MouseEvent event) {
        /*int x = event.getX();
        int y = event.getY();
        if (!event.isMetaDown()) {
            Graphics g = getGraphics();
            //g.drawLine(x, y, x2, y2);
            g.drawLine(x, y, x2, y2);
            g.dispose();
            x2 = x;
            y2 = y;

        }*/
        /*x2 = event.getX();
        y2 = event.getY();
        repaint();
*/
        x2 = event.getX();
        y2 = event.getY();
        //pos = -1;
        repaint();

    }

     }
      @Override
        public void paintComponent(Graphics graphic){
            super.paintComponent(graphic);
            //graphic.drawLine(x, y , x2, y2);
            //drawRect(graphic);
            drawLine(graphic, x, y, x2, y2);
            //drawElispe(graphic);
        }
    }


public class OnlyTestDrawshape extends JFrame implements ActionListener, Runnable{
        public static final int WIDTH = 900;
        public static final int HEIGHT = 700;
        private JPanel pn1One;
        private JPanel pn1Two;
        private JPanel pn1Three;
        private JPanel pn1Four;
        private JPanel pn1Five;
        private JMenu menu;
        private JMenuItem open, save, saveAs;
        private JLabel status, display;
        private JButton btnLine, btnRec, btnEli, btnDot;

        public OnlyTestDrawshape(String arg) throws HeadlessException{
            super(arg);
        }



        private void createGUI(){
            status = new JLabel();
            display = new JLabel();
            menu = new JMenu("File");
            JMenuBar menuBar = new JMenuBar();
            open = new JMenuItem("open");
            save = new JMenuItem("save");
            saveAs = new JMenuItem("save as");
            menu.add(open); menu.add(save); menu.add(saveAs);
            menuBar.add(menu);
            setJMenuBar(menuBar);
            setSize(400,400);
            setLayout(null);
            setSize(WIDTH, HEIGHT);
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setLayout(new BorderLayout());
            pn1One = createPanel(Color.WHITE);
            pn1Two= createPanel(Color.lightGray);
            pn1Three = createPanel(Color.lightGray);
            pn1Four = createPanel(Color.lightGray);
            pn1Five = createPanel(Color.lightGray);
          /*  btnLine = createButton("Line");
            btnRec = createButton("Rectangle");
            btnEli = createButton("Ellipse");
            btnDot = createButton("Dot");
            layoutButton();*/
            getContentPane().add(pn1One,BorderLayout.CENTER);
            getContentPane().add(pn1Two,BorderLayout.NORTH);
            getContentPane().add(pn1Three,BorderLayout.SOUTH);
            getContentPane().add(pn1Four,BorderLayout.EAST);
            getContentPane().add(pn1Five,BorderLayout.WEST);
            add(new DrawShape());
            pn1One.add(status);
            pn1One.add(display);
            repaint();
            setVisible(true);
        }


        private JPanel createPanel(Color C){
            JPanel panel = new JPanel();
            panel.setBackground(C);
            return panel;
        }
       /* private JButton createButton(String name){
            JButton button = new JButton(name);
            button.addActionListener(this);
            return button;
        }

        private void layoutButton(){
            GridBagLayout layout = new GridBagLayout();
            pn1Five.setLayout(layout);

            GridBagConstraints contraints = new GridBagConstraints();

            contraints.fill = GridBagConstraints.NONE;
            contraints.anchor = GridBagConstraints.CENTER;
            contraints.weightx = 100;
            contraints.weighty = 100;
            addToPanel(pn1Five, btnLine, contraints, 0,0,1,1);
            addToPanel(pn1Five, btnRec, contraints, 0,1,1,1);
            addToPanel(pn1Five, btnEli, contraints, 0,2,1,1);
            addToPanel(pn1Five, btnDot, contraints, 0,3,1,1);
        }

        private void addToPanel(JPanel panel, Component c, GridBagConstraints constraints, int x, int y, int w, int h){
            constraints.gridx = x;
            constraints.gridy = y;
            constraints.gridheight = h;
            constraints.gridwidth = w;
            panel.add(c, constraints);
        }
*/



        public void actionPerformed (ActionEvent e){

        }

        @Override
        public void run() {
            createGUI();
        }


    }


