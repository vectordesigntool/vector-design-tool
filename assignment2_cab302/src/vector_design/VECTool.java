package vector_design;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

public class VECTool extends JFrame implements ActionListener, Runnable{
    public static final int WIDTH = 900;
    public static final int HEIGHT = 700;

    private JFrame jf = new JFrame();
    private JPanel pn1One;
    private JPanel pn1Two;
    private JPanel pn1Three;
    private JPanel pn1Four;
    private JPanel pn1Five;
    private JPanel pnlSix;
    private FileMenu menu;
    private FileOperator open, save;
    private JLabel status, display;
    private int x, y, x2, y2, x4, y4;
    private int temp_XCOR,temp_YCOR;
    private JButton btnLine, btnRec, btnEli, btnDot, btnPoly, btnColor, btnFillcolor;
    private Boolean drawLine = false, drawRec = false, draweElli = false, drawDot = false, drawPoly = false;
    private Canvas canvas;
    private static ArrayList<Line2D> myLines = new ArrayList<Line2D>();
    private static ArrayList<Rectangle2D> rec = new ArrayList<Rectangle2D>();
    private static ArrayList<Line2D> myDot = new ArrayList<Line2D>();
    private static ArrayList<Ellipse2D> ellipse = new ArrayList<Ellipse2D>();


    private Rectangle2D tempRect = null;
    private Ellipse2D tempElli = null;

    private List<Point> pol = new ArrayList<Point>();
    private List<Polygon> poly = new ArrayList<Polygon>();
    private boolean finish = false;
    private boolean clean = false;
    private Polygon tempPoly = null;
    private Color m;





    public VECTool(String arg) throws HeadlessException{
        super(arg);
        MouseListener listener = new MouseListener();
        addMouseListener(listener);
        addMouseMotionListener(listener);
    }



    private void createGUI(){
        //Create a file chooser
        final JFileChooser fc = new JFileChooser();
        JColorChooser ColourSwatch = new JColorChooser();
        status = new JLabel();
        display = new JLabel();
        menu = new FileMenu("menu");
        JMenuBar menuBar = new JMenuBar();

        open = new FileOperator("open");
        open.addOpenListenr();
        save = new FileOperator("save");
        save.addSaveListener();

        menu.add(open); menu.add(save);
        menuBar.add(menu);
        setJMenuBar(menuBar);
        setSize(400,400);
        setLayout(null);
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        pn1One = createPanel(Color.WHITE);
        pn1Two= createPanel(Color.lightGray);
        pn1Three = createPanel(Color.lightGray);
        pn1Four = createPanel(Color.lightGray);
        pn1Five = createPanel(Color.lightGray);
        btnLine = createButton("Line");
        btnRec = createButton("Rectangle");
        btnEli = createButton("Ellipse");
        btnDot = createButton("Dot");
        btnPoly = createButton("Polygon");
        btnColor = createButton("Color");
        btnFillcolor = createButton("FillColor");
        layoutButton();
        getContentPane().add(pn1One,BorderLayout.CENTER);
        getContentPane().add(pn1Two,BorderLayout.NORTH);
        getContentPane().add(pn1Three,BorderLayout.SOUTH);
        getContentPane().add(pn1Four,BorderLayout.EAST);
        getContentPane().add(pn1Five,BorderLayout.WEST);
        pn1One.add(status);
        pn1One.add(display);
        repaint();
        setVisible(true);
        btnColor.addActionListener(new ActionListener() {
                                           JFrame jf;
                                           @Override
                                           public void actionPerformed(ActionEvent e) {
                                               Color c = JColorChooser.showDialog(jf, "S", Color.black);
                                               m = c;
                                               clean = false;
                                           }

                                       }
        );
        btnFillcolor.addActionListener(new ActionListener() {
                                       JFrame jf;
                                       @Override
                                       public void actionPerformed(ActionEvent e) {
                                           Color c = JColorChooser.showDialog(jf, "S", null);
                                           m = c;
                                           clean = true;
                                       }

                                   }
        );

    }

    public ArrayList getDotList(){
        return myDot;
    }
    public ArrayList getLineList(){
        return myLines;
    }
    public ArrayList getRectList(){
        return rec;
    }
    public ArrayList getEllipseList(){
        return ellipse;
    }


    private JPanel createPanel(Color C){
        JPanel panel = new JPanel();
        panel.setBackground(C);
        return panel;
    }
    private JButton createButton(String name){
        JButton button = new JButton(name);
        button.addActionListener(this);
        return button;
    }

    private void layoutButton(){
        GridBagLayout layout = new GridBagLayout();
        pn1Five.setLayout(layout);

        GridBagConstraints contraints = new GridBagConstraints();

        contraints.fill = GridBagConstraints.NONE;
        contraints.anchor = GridBagConstraints.CENTER;
        contraints.weightx = 100;
        contraints.weighty = 100;
        addToPanel(pn1Five, btnLine, contraints, 0,0,1,1);
        addToPanel(pn1Five, btnRec, contraints, 0,1,1,1);
        addToPanel(pn1Five, btnEli, contraints, 0,2,1,1);
        addToPanel(pn1Five, btnDot, contraints, 0,3,1,1);
        addToPanel(pn1Five, btnPoly, contraints, 0,4,1,1);
        addToPanel(pn1Five, btnColor, contraints, 0,5,1,1);
        addToPanel(pn1Five, btnFillcolor, contraints, 0,6,1,1);
    }

    private void addToPanel(JPanel panel, Component c, GridBagConstraints constraints, int x, int y, int w, int h){
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridheight = h;
        constraints.gridwidth = w;
        panel.add(c, constraints);
    }


    public void drawDot(Graphics g, double x, double y){
        Graphics2D draw = (Graphics2D) g;
        /*Line2D l = new Line2D.Double(x, y, x, y);
        l.setLine(x, y, x, y);
        draw.draw(l);*/
        draw.draw(new Line2D.Double(x, y, x, y));
        for (Line2D d : myDot){
            draw.draw(d);
        }

    }

    private void drawLine(Graphics g, double x, double y, double x2, double y2) {
            Graphics2D draw = (Graphics2D) g;

            draw.draw(new Line2D.Double(x, y, x2, y2));
            for (Line2D l : myLines){

                draw.draw(l);
            }
    }
    private Rectangle2D drawRect(double x, double y, double x2, double y2) {
        Rectangle2D rect = new Rectangle2D.Double(x, y, x2, y2);
        rect.setFrameFromDiagonal(x , y, x2, y2);
        return  rect;

    }

    private Ellipse2D drawElli(double x, double y, double x2, double y2) {
        //Graphics2D g2 = (Graphics2D) g;
        Ellipse2D ellipse = new Ellipse2D.Double(x, y, x2, y2);
        ellipse.setFrameFromDiagonal(x, y, x2, y2);
        //g2.draw(ellipse);
        return ellipse;
    }


    private void drawPoly(Graphics g, List<Point> pol){
        Polygon po = new Polygon();
        for (int i = 0; i < pol.size() - 1; i++) {
            Point po1 = pol.get(i);
            Point po2 = pol.get(i + 1);
            //g.drawLine(po1.x, po1.y, po2.x, po2.y);
            po.addPoint(po1.x, po1.y);
            poly.add(po);
        }

        if(finish == true){
            //g.drawLine(pol.get(0).x, pol.get(0).y, pol.get(pol.size() - 1).x, pol.get(pol.size() - 1).y);
            pol.clear();
            finish = false;
        }
        if (clean == true){
            g.fillPolygon(po);
        }

        g.drawPolygon(po);

    }



    private class MouseListener extends MouseAdapter {


        public void mousePressed(MouseEvent event) {

            x = x2 = event.getX();
            y = y2 = event.getY();
            status.setText("(" + x + "," + y + ")");

            repaint();

        }

        public void mouseClicked(MouseEvent e) {


            pol.add(e.getPoint());
            x4 = e.getX();
            y4 = e.getY();
            if(e.getClickCount() == 2){
                finish = true;


            }
            display.setText("(" + x4 + "," + y4 + ")");
            repaint();
        }


        public void mouseDragged(MouseEvent event) {
            x2 = event.getX();
            y2 = event.getY();
            display.setText("(" + x2 + "," + y2 + ")");
            tempRect = drawRect(x, y, x2, y2);
            tempElli = drawElli(x, y, x2, y2);
            repaint();

        }

        public void mouseReleased(MouseEvent event) {

            x2 = event.getX();
            y2 = event.getY();
            if(drawLine) {
                myLines.add(new Line2D.Double(x, y, x2, y2));
            }
            if(drawDot){
                myDot.add(new Line2D.Double(x,y, x,y));
            }
            if (drawRec){
                rec.add(drawRect(x, y, x2, y2));
            }
            if (draweElli){
                ellipse.add(drawElli(x, y, x2, y2));
            }

            repaint();
        }

    }

    @Override
    public void paint(Graphics graphic){
        super.paint(graphic);
        Graphics2D g = (Graphics2D) graphic;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
               RenderingHints.VALUE_ANTIALIAS_ON);

        g.setColor(m);


        if(drawRec) {
            if(clean == true){
                for (Rectangle2D rectangle : rec){
                    g.fill(rectangle);
                }
            }
            if(tempRect != null){
                g.draw(tempRect);
            }
            for (Rectangle2D rectangle : rec){
                g.draw(rectangle);
            }
        }
        if (drawLine) {
            g.setColor(m);
            drawLine(graphic, x, y, x2, y2);
        }
        if (draweElli) {
            if(clean == true){
                for  (Ellipse2D ellipse2D : ellipse) {
                    g.fill(ellipse2D);
                }
            }
            if(tempElli != null){
                g.draw(tempElli);
            }
            for  (Ellipse2D ellipse2D : ellipse){
                g.draw(ellipse2D);
            }

        }
        if (drawDot) {
            drawDot(graphic, x, y);

        }
        if(drawPoly){
            drawPoly(graphic, pol);

        }


    }

    public void actionPerformed (ActionEvent e){
        Object src = e.getSource();
        if(src == btnLine){
            drawLine = true;
            drawDot = false;
            draweElli = false;
            drawRec = false;
            drawPoly = false;
        } else if(src == btnDot){
            drawDot = true;
            draweElli = false;
            drawRec = false;
            drawLine = false;
            drawPoly = false;
        } else if(src == btnRec){
            drawRec = true;
            draweElli = false;
            drawDot = false;
            drawLine = false;
            drawPoly = false;
        } else if(src == btnEli){
            draweElli = true;
            drawRec = false;
            drawDot = false;
            drawLine = false;
            drawPoly = false;
        } else if (src == btnPoly){
            drawDot = false;
            draweElli = false;
            drawRec = false;
            drawLine = false;
            drawPoly = true;
        }
    }

    @Override
    public void run() {
        createGUI();
    }
    public static void main(String[] args){
        JFrame.setDefaultLookAndFeelDecorated(true);
        SwingUtilities.invokeLater(new VECTool("Vector Design Tool"));

    }

}
