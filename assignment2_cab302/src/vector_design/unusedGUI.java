package vector_design;

import javax.swing.*;
import java.awt.*;

public class unusedGUI {

    private unusedGUI() {
        //Initializes frame, menu, bottom/colour panel, sidebar and color swatch
        JFrame MainFrame = new JFrame("Group 130 - Vector Design Tool");
        JMenuBar TopMenuBar = new JMenuBar();
        JPanel ColourPanel = new JPanel();
        JPanel SideBar = new JPanel();
        JColorChooser ColourSwatch = new JColorChooser();

        //Initializes shape buttons for sidebar
        JButton Line = new JButton("Line");
        JButton Plot = new JButton("Plot");
        JButton Rectangle = new JButton("Rectangle");
        JButton Ellipse = new JButton("Ellipse");
        JButton Polygon = new JButton("Polygon");

        //Sets up File menu button and subsequent items
        JMenu File = new JMenu("File");
        JMenuItem F1 = new JMenuItem("New");
        JMenuItem F2 = new JMenuItem("Open");
        JMenuItem F3 = new JMenuItem("Save");
        JMenuItem F4 = new JMenuItem("Save As");
        File.add(F1);
        File.add(F2);
        File.add(F3);
        File.add(F4);
        TopMenuBar.add(File);

        //Sets up Edit menu button and subsequent items
        JMenu Edit = new JMenu("Edit");
        JMenuItem E1 = new JMenuItem("Cut");
        JMenuItem E2 = new JMenuItem("Copy");
        JMenuItem E3 = new JMenuItem("Paste");
        JMenuItem E4 = new JMenuItem("Select All");
        Edit.add(E1);
        Edit.add(E2);
        Edit.add(E3);
        Edit.add(E4);
        TopMenuBar.add(Edit);

        //Sets up View menu button and subsequent items
        JMenu View = new JMenu("View");
        JMenuItem V1 = new JMenuItem("Zoom In");
        JMenuItem V2 = new JMenuItem("Zoom Out");
        JMenuItem V3 = new JMenuItem("Zoom to 100%");
        JMenuItem V4 = new JMenuItem("Full Screen Preview");
        View.add(V1);
        View.add(V2);
        View.add(V3);
        View.add(V4);
        TopMenuBar.add(View);

        //Sets colour of bottom panel and adds colour swatch
        ColourPanel.setBackground(Color.LIGHT_GRAY);
        ColourPanel.add(ColourSwatch);

        //Configures attributes for sidebar (incl. colour, size and buttons)
        SideBar.setBackground(Color.LIGHT_GRAY);
        SideBar.setPreferredSize(new Dimension(100,768));
        SideBar.add(Line);
        SideBar.add(Plot);
        SideBar.add(Rectangle);
        SideBar.add(Ellipse);
        SideBar.add(Polygon);

        //Sets up other attributes for the VECTool frame
        //Includes adding canvas, colour panel, side bar, menu bar, setting size, keeping visibility and ensuring a graceful exit
        MainFrame.add(new CanvasMain());
        MainFrame.add(ColourPanel, BorderLayout.SOUTH);
        MainFrame.add(SideBar, BorderLayout.WEST);
        MainFrame.setJMenuBar(TopMenuBar);
        MainFrame.setSize(1024, 768);
        MainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainFrame.setVisible(true);
    }

    //Create Canvas
    public class CanvasMain extends Canvas {
        //Set the colour of the canvas
        public CanvasMain() {
            setBackground(Color.WHITE);
        }

        //Test canvas implementation with a pre-determined filled oval shape
        //DELETE THIS CODE BEFORE IMPLEMENTING THE EXISTING SHAPE CODE FROM THE OTHER FILES
        public void paint (Graphics g) {
            g.fillOval(75,75,150,75);
        }
    }

    //Main method which creates a new instance of the unusedGUI
    public static void main(String[] args) {
       new unusedGUI();
   }
}
